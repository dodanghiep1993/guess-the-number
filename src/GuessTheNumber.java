import java.util.MissingFormatArgumentException;
import java.util.Random;
import java.util.Scanner;

public class GuessTheNumber {
    public static void main(String[] args) {
        String answer = "";
        do {
            Scanner getInput = new Scanner(System.in);
            Random rand = new Random();
            int tries = 6;
            int random = rand.nextInt(20) + 1;
            System.out.println("\n" + "Hello! What is your name?\n" +
                    "\n");

            String name;
            try {
                if (getInput.hasNextInt()) {
                    throw new Exception();
                } else {
                    name = getInput.nextLine();
                }
            } catch (Exception e) {
                System.out.println();
                System.out.println("\n" + "Invalid input! Names should be characters");
                break;
            }
            System.out.println("\n");

            System.out.println("Well, " + name + ", I am thinking of a number between 1 and 20.\n" +
                    "\n" + "Take a guess.\n" + "\n");

            int num;
            try {
                num = getInput.nextInt();
            } catch (Exception e) {
                System.out.println();
                System.out.println();
                System.out.println("Invalid input! input should be Integers");
                break;
            }
            System.out.println("\n");

            for (int i = 0; i < tries; i++) {
                if (num == random) {
                    System.out.println("Good job, " + name + "! You guessed my number in " + (i+1) + " guesses!\n" +
                            "\n" +
                            "Would you like to play again? (y or n)\n" + "\n");
                    try {
                        if (getInput.hasNextInt()) {
                            throw new Exception();
                        } else {
                            answer = getInput.nextLine();
                        }
                    } catch (Exception e) {
                        System.out.println();
                        System.out.println("\n" + "Invalid input! input should be y or n");
                        break;
                    }
                    break;
                } else if (i == tries - 1) {
                    System.out.println("Sorry! " + name + "! You have tried all " + tries + " guesses!\n" +
                            "\n" +
                            "Would you like to play again? (y or n)\n" + "\n");
                    try {
                        if (getInput.hasNextInt()) {
                            throw new Exception();
                        } else {
                            answer = getInput.nextLine();
                        }
                    } catch (Exception e) {
                        System.out.println();
                        System.out.println("\n" + "Invalid input! input should be y or n");
                        break;
                    }
                } else if (num > random) {
                    System.out.println("Your guess is too high.\n" +
                            "\n" +
                            "Take a guess.\n" + "\n");
                    try {
                        num = getInput.nextInt();
                    } catch (Exception e) {
                        System.out.println();
                        System.out.println();
                        System.out.println("Invalid input! input should be Integers");
                        break;
                    }
                    System.out.println("\n");
                } else {
                    System.out.println("Your guess is too low.\n" +
                            "\n" +
                            "Take a guess.\n" + "\n");
                    try {
                        num = getInput.nextInt();
                    } catch (Exception e) {
                        System.out.println();
                        System.out.println();
                        System.out.println("Invalid input! input should be Integers");
                        break;
                    }
                    System.out.println("\n");
                }
            }
        } while (answer.equals("y"));
    }
}
